﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColocarNumUI : MonoBehaviour
{ 
    public List<Text> numeros;
    List<int> listaNum;
    private List<int> listaNum2;
    int menorDeTodos;

    public UnityEngine.Vector2 posInicial;
    const int ANCHO_TEXTO = 100;
    static UnityEngine.Vector2 DIMENSIONES = new Vector2(ANCHO_TEXTO, 0);

    // Start is called before the first frame update
    void Start()
    {
        this.listaNum2 = new List<int>();
        this.listaNum = new List<int>();

        foreach (Text txtNum in this.numeros)
        {
            int num = Int32.Parse(txtNum.text);
            this.listaNum.Add(num);
        }
        
        // Es lo mismo que:
        for (int i = 0; i < numeros.Count; i++)
        {
            Text txtNum = numeros[i];
            // int num = Int32.Parse(txtNum.text); ....
        }
        menorDeTodos = OrdenarComoHumano.Menor(listaNum);
       // menorDeTodos = OrdenarComoHumano.Menor(listaNum2);
        OrdenarComoHumano.MostrarLista(listaNum);
        ColocarSeguidos();
    }
    public void ColocarSeguidos()
    {
        this.posInicial = this.numeros[0].transform.GetComponent<RectTransform>().localPosition;
          
       /* Vector2 nuevaPos2 = new Vector2(this.posInicial.x + ANCHO_TEXTO, this.posInicial.y);
        this.numeros[1].GetComponent<RectTransform>().localPosition = nuevaPos2;

        Vector2 nuevaPos3 = new Vector2(nuevaPos2.x + ANCHO_TEXTO, this.posInicial.y);
        this.numeros[2].GetComponent<RectTransform>().localPosition = nuevaPos3;

        Vector2 nuevaPos4 = new Vector2(nuevaPos3.x + ANCHO_TEXTO, this.posInicial.y);
        this.numeros[3].GetComponent<RectTransform>().localPosition = nuevaPos4;*/

        for (int i = 1; i < this.numeros.Count; i++)
        {
            float nuevaPosX =  this.numeros[i - 1].rectTransform.localPosition.x / 2 + ANCHO_TEXTO;
            float nuevaPosY = this.posInicial.y;
            Vector2 nuevaPos = new Vector2(nuevaPosX, nuevaPosY);
            this.numeros[i].rectTransform.localPosition = nuevaPos;
        }
    }
    /*  public void ColocarSeguidos()
      {
          List<int> listaPosX = new List<int>();

          foreach (Text txtNum in this.numeros)
          {
              int posX = (int)txtNum.rectTransform.localPosition.x;
              listaPosX.Add(posX);
          }
          int posMenorX = OrdenarComoHumano.Menor(listaPosX);
          this.posInicial = new Vector2(posMenorX, this.numeros[0].rectTransform.rect.position.y);

          this.numeros[0].rectTransform.localPosition = this.posInicial;

          for (int i = 1; i < this.numeros.Count; i = i + 1 ) // i++ 
          {
              // Vector2 posSiguiente = this.posInicial + DIMENSIONES;
              int posSiguiente = posMenorX + ANCHO_TEXTO;
              Vector2 vPosSig = new Vector2(posSiguiente, this.numeros[i].rectTransform.position.y);

          }
      }*/
}
