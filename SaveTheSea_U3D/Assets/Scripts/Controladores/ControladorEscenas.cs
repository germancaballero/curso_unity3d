﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Comentario de pruba
namespace GermanSTS
{
    // Clase ControladorEscenas
    public class ControladorEscenas : MonoBehaviour
    {
        [Header("Propiedades ctrl escenas:")]
        public string nombreEscena;

        /// Ejemplo de recursividad
        void Imprimir(int x)
        {
            if (x > 0)
            {
                Debug.Log("Entrando: " + x);
                Imprimir(x - 1);
            }
            Debug.Log("Saliendo: " + x);
        }

        private void Start()
        {
            /// Ejemplo de recursividad
            // Imprimir(5);
            if (nombreEscena != "")
            {
                print("Arrancando escena " + nombreEscena);
                this.CargarEscena(nombreEscena);
            }
            AsignarCerrarAlBotonSalir();
        }
        // Update is called once per frame
        void Update()
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                CerrarAplicacion();
            }
        }
        public void CargarEscena(string escena)
        {
            SceneManager.LoadScene(escena);
        }
        // Vamos a centrarlizar aquí la funcionalidad de cuando se cierre la aplicación
        public void CerrarAplicacion()
        {
            Application.Quit();
        }
        void AsignarCerrarAlBotonSalir()
        {
            GameObject objCanvas = GameObject.Find("Canvas");
            if (objCanvas != null)
            {
                Transform canvas = objCanvas.GetComponent<Transform>();
                Transform button_Salir_Si = null;
                FindByNameInactives(canvas, "Button_Salir_Si", ref button_Salir_Si);

                if (button_Salir_Si != null)
                {
                    button_Salir_Si.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(this.CerrarAplicacion);
                }
            }
        }
        void FindByNameInactives(Transform raiz, string nombre, ref Transform objetoEncontrado)
        {
            for (int i = 0; i < raiz.childCount; i++)
            {
                Transform objTransf = raiz.GetChild(i);
                if (objTransf.name == nombre)
                {
                    objetoEncontrado = objTransf;
                    return;
                } else
                {
                    FindByNameInactives(objTransf, nombre, ref objetoEncontrado);
                }
            }
        }
    }
}
