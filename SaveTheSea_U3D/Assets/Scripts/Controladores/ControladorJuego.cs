﻿using UnityEngine;
using System.Collections;

public class ControladorJuego: MonoBehaviour
{
    [Header("VIDA:")]
    public int vidas = 7;
    [Header("Prefab y obj juego:")]
    public GameObject[] prefabsEnemigos;
    public GameObject esqSuperior;
    [Header("Objetos UI:")]
    public GameObject panelFinJuego;
    public GameObject panelGanador;
    public GameObject panelPerdedor;
    public UnityEngine.UI.Text textoVidas;
    public UnityEngine.UI.Text textoPuntos;
    [Header("Listado de enemigos:")]
    public AparicionEnemigo[] apariciones;

    int puntos = 0;
    int enemigoActual;
    float timeInicio;

    // Use this for initialization
    void Start()
    {
        textoPuntos.text = "" + this.puntos;
        textoVidas.text = "" + this.vidas;

        /* Unity se encarga de hacer esto en el inspector
         * this.apariciones = new AparicionEnemigo[0];
        this.apariciones[0] = new AparicionEnemigo(2, 1);
        this.apariciones[1] = new AparicionEnemigo(-3, 6);
        this.apariciones[2] = new AparicionEnemigo(7, 10); */
        enemigoActual = 0;
        timeInicio = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        float tiempoActual = Time.time - timeInicio;
        if (enemigoActual < apariciones.Length)
        {
            // Si el tiempo que ha pasado es mayor que cuando se supone que debe
            // aparecer el enemigo actual, entonces...
            if (tiempoActual > apariciones[enemigoActual].tiempoInicio)
            {
                int indiceEnem = apariciones[enemigoActual].indiceEnemigo;
                GameObject prefabEnemigo = prefabsEnemigos[indiceEnem];

                // Instanciamos el enemigo
                GameObject enemigo = GameObject.Instantiate(prefabEnemigo);
                float posX = apariciones[enemigoActual].posInicioX;
                enemigo.transform.position = new Vector3(posX, esqSuperior.transform.position.y, 0);

                enemigo.GetComponent<Enemigo>().controlJuego = this;
                enemigo.GetComponent<Enemigo>().numEnemigo = enemigoActual;
                enemigoActual++;
            }
        }
    }
    public void SumarPuntos(int puntos, int numEnem)
    {
        this.puntos += puntos;
        // Mostramos puntos
        textoPuntos.text = "" + this.puntos;
        Debug.Log("Enemigos " + enemigoActual);
        ComprobarFinJuego(numEnem);
    }
    public void RestarVida(int numEnem)
    {
        // Restar vidas
        this.vidas--;
        // Y si hemos perdido, mostrar el panel correspondiente
        ComprobarFinJuego(numEnem);
        // Mostramos puntos y vidas
        textoVidas.text = "" + this.vidas;
    }
    private void ComprobarFinJuego(int numEnem)
    {
        if (this.vidas <= 0)
        {
            panelFinJuego.SetActive(true);
            panelPerdedor.SetActive(true);
            this.enabled = false;
            this.vidas = 0;
        } else if (numEnem == this.apariciones.Length - 1)
        {
            panelFinJuego.SetActive(true);
            panelGanador.SetActive(true);
            this.enabled = false;
        }
    }
}
