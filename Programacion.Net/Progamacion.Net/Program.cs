﻿using System;

namespace Progamacion.Net
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            int sumar = 0;
            bool siContinuar = true;

            while (siContinuar)
            {
                int valor = 10;
                sumar += valor;
                string continuar = Console.ReadLine();
            }
        }
    }
}
