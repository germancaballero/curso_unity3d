﻿using System;

namespace Leccion11_Ejercicio01
{
    class Program
    {
        //Realizar un programa que acumule (sume) valores ingresados por teclado hasta ingresar el 9999 (no sumar dicho valor, indica que ha finalizado la carga). Imprimir el valor acumulado e informar si dicho valor es cero, mayor a cero o menor a cero. 
        static void Main(string[] args)
        {
            int acumulador = 0, num;

            do
            {
                Console.WriteLine("Mete un número (9999 para terminar)");
                num = int.Parse(Console.ReadLine());
                acumulador += (acumulador != 9999) ? num : 0;
            } while (num != 9999);

            Console.WriteLine("Suma total: " + acumulador);

            if (acumulador > 0)
                Console.WriteLine("Suma total es mayor cero");
            else if (acumulador < 0)
                Console.WriteLine("Suma total es menor cero");
            else
                Console.WriteLine("Suma total es igual cero");
             
        }
    }
}
