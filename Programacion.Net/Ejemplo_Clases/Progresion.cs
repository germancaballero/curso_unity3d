﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplos_Clases
{
    class Progresion
    {
        public int x;
        public int y;

        public void CargarDatos()
        {
            Console.WriteLine("Introduzca el número que se va a multiplicar");
            x = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Introduzca cuantas veces se va a repetir el bucle");
            y = Int32.Parse(Console.ReadLine());
        }
        public void GenerarSerie()
        {
            Console.WriteLine("\n " + x + " veces " + y);
            for (int contador = 1; contador <= y; contador++)
            {
                Console.Write(contador * x + " - ");
            }
        }
    }
}
