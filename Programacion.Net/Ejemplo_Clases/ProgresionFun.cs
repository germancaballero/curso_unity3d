﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplos_Clases
{
    class ProgresionFun
    {
        // Mejora 1: Programación funcional: Con una función, en C#, una función "pura" es un MÉTODO ESTÁTICO
        public static void MostrarProgresion(int x, int y)
        {
            Console.WriteLine("\n " + x + " veces " + y);
            for (int contador = 1; contador <= x; contador++)
                Console.Write(contador * y + " - ");
        }
    }
}
