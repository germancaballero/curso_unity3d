﻿using System;


namespace Ejemplos_Clases
{
    class Program
    {
        static void Main()
        {
            Progresion nuevaSecuencia = new Progresion();
            nuevaSecuencia.x = 11;
            nuevaSecuencia.y = 25;
            nuevaSecuencia.GenerarSerie();
            nuevaSecuencia.x = 7;
            nuevaSecuencia.y = 3;
            nuevaSecuencia.GenerarSerie();

            nuevaSecuencia.x = 13;
            nuevaSecuencia.y = 9;
            nuevaSecuencia.GenerarSerie();

            nuevaSecuencia.CargarDatos();
            nuevaSecuencia.GenerarSerie();
            nuevaSecuencia.GenerarSerie();
            nuevaSecuencia.GenerarSerie();
            nuevaSecuencia.GenerarSerie();

            Progresion secuenciasPares = new Progresion();
            secuenciasPares.x = 2;
            secuenciasPares.y = 5;
            secuenciasPares.GenerarSerie();
            secuenciasPares.x = 4;
            secuenciasPares.GenerarSerie();
            secuenciasPares.x = 6;
            secuenciasPares.GenerarSerie();
            secuenciasPares.x = 8;
            secuenciasPares.GenerarSerie();

        }

        // Este va 25 veces, de 11 en 11 a partir del 11.
        static void MainPorFunciones(string[] args)
        {
            Console.WriteLine("25 veces 11");
            // for (int contador = 1; contador <= 25; contador++)
            //    Console.Write(contador * 11 + " - ");
            ProgresionFun.MostrarProgresion(25, 11);

            Console.WriteLine("\n7 veces 3");
            ProgresionFun.MostrarProgresion(7, 3);

            Console.WriteLine("\n13 veces 9");
            ProgresionFun.MostrarProgresion(13, 9);

            Console.WriteLine("\nIntroduzca veces y repetición:");
            int veces = int.Parse(Console.ReadLine());
            int repe = int.Parse(Console.ReadLine());
            ProgresionFun.MostrarProgresion(veces, repe);
        }
        // Ahora que vaya X veces, de Y en Y, a partir de Y. 
        // Donde X, e Y, pueden ser muchos tipos. Por ejemplo. 
        // Haz para el X= 7, Y = 3.
        //  X= 13, Y = 9
        // Para X= lo que el usuario meta, e Y también
        // Imaginaros otros 20 casos

    }

}
