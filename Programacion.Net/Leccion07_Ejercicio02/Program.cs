﻿using System;

namespace Leccion07_Ejercicio02
{
    //Se ingresa por teclado un valor entero, mostrar una leyenda que indique si el número es positivo, nulo o negativo. 
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Numero: ");
            int valor = int.Parse(Console.ReadLine());
            if (valor < 0)
            {
                Console.WriteLine("Es negativo");
            }
            else if (valor == 0)
            {
                Console.WriteLine("Es cero");
            }
            else
            {
                Console.WriteLine("Es positivo");
            }
        }
    }
}
