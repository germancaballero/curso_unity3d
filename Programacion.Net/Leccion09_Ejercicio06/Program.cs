﻿using System;
using System.Globalization;

namespace Leccion09_Ejercicio06
{
    /* Realizar un programa que permita cargar dos listas de 15 valores cada una. 
     * Informar con un mensaje cual de las dos listas tiene un valor acumulado mayor (mensajes "Lista 1 mayor", "Lista 2 mayor", "Listas iguales")
    Tener en cuenta que puede haber dos o más estructuras repetitivas en un algoritmo.
    */
    class Program
    {
        const int NUM = 3;

        static void Main(string[] args)
        {
            int acum1 = 0, acum2 = 0;
            int pos = 0;
            while (pos < NUM)
            {
                Console.WriteLine("Lista 1, Pon algo: ");
                acum1 += int.Parse(Console.ReadLine());
                pos++;
            }
            pos = 0;
            while (pos < NUM)
            {
                Console.WriteLine("Lista 2, Pon algo: ");
                acum2 += int.Parse(Console.ReadLine());
                pos++;
            }
            Console.WriteLine(acum1 > acum2 ? "Lista 1 mayor" + acum1
                                            : acum1 < acum2 ? "Lista 2 mayor" + acum2
                                                            : "Son iguales");

            if (acum1 > acum2)
            {
                Console.WriteLine("Lista 1 mayor");
            }
            else if (acum1 < acum2)
            {
                Console.WriteLine("Lista 2 mayor");
            }
            else
            {
                Console.WriteLine("igual");
            }
        }
    }
}
