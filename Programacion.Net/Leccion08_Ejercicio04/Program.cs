﻿using System;

namespace Leccion08_Ejercicio04
{
    class Program
    {
        //Se ingresan por teclado tres números, si al menos uno de los valores ingresados es menor a 10, imprimir en pantalla la leyenda "Alguno de los números es menor a diez". 
        static void Main(string[] args)
        {
            Console.WriteLine("Numero 1: ");
            int valor1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Numero 2: ");
            int valor2 = int.Parse(Console.ReadLine());
            Console.WriteLine("Numero 3: ");
            int valor3 = int.Parse(Console.ReadLine());

            if (valor1 < 10 || valor2 < 10 || valor3 < 10)
            {
                Console.WriteLine("Alguno de los números es menor a diez");
            }
            // Otras maneras
            // if (  ! (valor1 >= 10 && valor2 >= 10 && valor3 >= 10))

/* 
bool algunoEsMenor = false;
if (valor1 < 10 )
{
    algunoEsMenor = true;
}
if (valor2 < 10)
{
    algunoEsMenor = true;
}
if (valor3 < 10)
{
    algunoEsMenor = true;
}
if (algunoEsMenor)
{
    Console.WriteLine("Alguno de los números es menor a diez");
}*/
}
}
}
