﻿using System;

namespace Leccion10_Ejercicio05
{
    class Program
    {
        /*
         * Realizar un programa que lea los lados de n triángulos, e informar:
a) De cada uno de ellos, qué tipo de triángulo es: equilátero (tres lados iguales), isósceles (dos lados iguales), o escaleno (ningún lado igual)
b) Cantidad de triángulos de cada tipo.
c) Tipo de triángulo que posee menor cantidad. 
        */
        static void Main(string[] args)
        {
            Console.WriteLine("Cuantos triangulos");
            int n = int.Parse(Console.ReadLine());
            int equilateros = 0, isosceles = 0, esc = 0;

            for (int tri = 0; tri < n; tri++)
            {
                Console.WriteLine("Lado 1");
                int l_1 = int.Parse(Console.ReadLine());
                Console.WriteLine("Lado 2");
                int l_2 = int.Parse(Console.ReadLine());
                Console.WriteLine("Lado 3");
                int l_3 = int.Parse(Console.ReadLine());

                if (l_1 == l_2 && l_2 == l_3)
                {
                    Console.WriteLine("equilátero");
                    equilateros++;
                }
                else if (l_1 == l_2 || l_2 == l_3 || l_1 == l_3)
                {
                    Console.WriteLine("isósceles");
                    isosceles++;
                }
                else
                {
                    Console.WriteLine("escaleno");
                    esc++;
                }
            }
            Console.WriteLine("equiláteros " + equilateros);
            Console.WriteLine("isósceless " + isosceles);
            Console.WriteLine("escalenoss  " + esc);
            if (equilateros == isosceles && equilateros == esc)
            {
                Console.WriteLine("Hay iguales");
            }
            else if (equilateros == isosceles)
            {
                Console.WriteLine("Hay iguales equi e isos");
            }
            else if (equilateros == esc)
            {
                Console.WriteLine("Hay iguales equi y esc");
            }
            else if (isosceles == esc)
            {
                Console.WriteLine("Hay iguales isos y esc");
            }
            else if (equilateros < isosceles && equilateros < esc)
            {
                Console.WriteLine("equiláteros hay menos");
            }
            else if (isosceles < esc)
                Console.WriteLine("isósceless hay menos");
            else
                Console.WriteLine("escalenoss hay menos");
        }
    }
}
