﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PruebaOrdenamiento : MonoBehaviour
{
    public int[] numeros;
    public int[] numOrdenados;

    // Start is called before the first frame update
    void Start()
    {
        numOrdenados = new int[numeros.Length];

        int masGrande = 0;

        // 1er PASO: Encontrar el más pequeño y el más grande sólo se ejecuta una vez
        for (int i = 0; i < numeros.Length; i++)
        {
            if (numeros[i] > masGrande)
            {
                masGrande = numeros[i];
            }
        }
        int masPeque = masGrande;
        for (int i = 0; i < numeros.Length; i++)
        {
            if (numeros[i] < masPeque)
            {
                masPeque = numeros[i];
            }
        }
        numOrdenados[0] = masPeque;

        // 2do PASO:
        
        for (int j = 0; j < numeros.Length - 1; j++)
        {
            // masPeque = masPeque + 1;    // masPeque++
            masPeque = masGrande;

            for (int i = 0; i < numeros.Length; i++)
            {
                // Para que pase el anterior encontrado
                if (numeros[i] > numOrdenados[j])
                {
                    //  
                    if (numeros[i] <= masPeque)
                    {
                        masPeque = numeros[i];
                        //TODO: Comprobar si el número está repetido, y en ese caso
                        // repetirlo en el array final.
                    }
                }
            }
            numOrdenados[j + 1] = masPeque;
        }
    }
}
