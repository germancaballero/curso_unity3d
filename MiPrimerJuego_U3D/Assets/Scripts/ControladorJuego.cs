﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorJuego : MonoBehaviour
{
    public GameObject[] enemigos;
    public AparicionEnemigo[] aparacionesEnem;

    public int vidas = 7;
    public int puntos = 0;
    public GameObject textoVidas;
    public GameObject textoPuntos;
    public GameObject panelGanador;
    public GameObject panelPerdedor;

    private float timeIni;
    private int enemActual;
    private int totalEnemFiniq;

    // Start is called before the first frame update
    void Start(  )
    {
        timeIni = Time.time;
        enemActual = 0;
        totalEnemFiniq = 0;
    }
    // Update is called once per frame
    void Update()
    {
        textoVidas.GetComponent<Text>().text = "Vidas: " + this.vidas;
        textoPuntos.GetComponent<Text>().text = "Puntos: " + this.puntos;
        // Para saber si un enem tiene que aparecer, tenemos que saber cuanto 
        // tiempo a pasado desde el inicio del nivel hasta el momento actual
        // (frame actual) y si es superior al tiempo configurado en la aparicion
        float tiempoActual = Time.time - timeIni;

        if (enemActual < aparacionesEnem.Length)
        {
            if (tiempoActual > aparacionesEnem[enemActual].tiempoInicio)
            {
                // Si NO ha aparecido...
                if (!aparacionesEnem[enemActual].yaHaAparecido)
                {
                    this.InstanciarEnemigo();
                    aparacionesEnem[enemActual].yaHaAparecido = true;
                    enemActual++;
                }
            }
        } else if (totalEnemFiniq >= aparacionesEnem.Length)
        {
            if (vidas > 0)
            {
                HasGanado();
            } else
            {
                HasPerdido();
            }
        }
    }
    public void HasGanado()
    {
        panelGanador.SetActive(true);
        panelPerdedor.SetActive(false);
        this.enabled = false;
        
    }
    public void HasPerdido()
    {
        panelGanador.SetActive(false);
        panelPerdedor.SetActive(true);
        this.enabled = false;
    }
    // Esto es un nuevo método 
    // (acción, conjunto de instrucciones, función, procedimiento, mensaje...)
    public void CuandoCapturamosEnemigo()
    {
        totalEnemFiniq++;
        // Estamos asignando un nuevo valor a la puntuación,
        // que es los puntos actuales + 10 ptos.
        this.puntos = this.puntos + 10;     // this.puntos  += 10;
        // this.InstanciarEnemigo();
    }
    public void CuandoPerdemosEnemigo()
    {
        totalEnemFiniq++;
        this.vidas = this.vidas - 1;        // this.vidas -= 1;   this.vidas--;
        // this.InstanciarEnemigo();
        if (this.vidas <= 0)
        {
            HasPerdido();
        }
    }
    public void InstanciarEnemigo()
    {
        int indiceEnem = aparacionesEnem[enemActual].indiceEnem;
        GameObject enem = GameObject.Instantiate(enemigos[indiceEnem]);
        float posX = aparacionesEnem[enemActual].posInicioX;
        enem.transform.position = new Vector3(posX, 10, 1);
    }
}
